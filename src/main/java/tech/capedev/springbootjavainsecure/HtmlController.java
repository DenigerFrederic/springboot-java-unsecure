package tech.capedev.springbootjavainsecure;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HtmlController {

    private final ArticleRepository articleRepository;
    private final UserRepository userRepository;

    public HtmlController(ArticleRepository articleRepository, UserRepository userRepository) {
        this.articleRepository = articleRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    public String blog(Model model){
        int value=Double.valueOf(20).intValue();
        model.addAttribute("title","[Springboot- Java] Articles with security issues");
        model.addAttribute("banner","Articles");
        model.addAttribute("articles",articleRepository.findAllByOrderByAddedAtDesc());
        return "blog";
    }

    @GetMapping("/user")
    public String user(@RequestParam("login") String login, Model model){
        model.addAttribute("title",login);
        model.addAttribute("user", userRepository.findByLoginUnsecure(login));
        return "user";
    }

    @GetMapping("/article")
    public String article(@RequestParam("slug") String slug, Model model){
        Article article=articleRepository.findBySlug(slug);
        if(article==null){
            model.addAttribute("slug",slug);
            model.addAttribute("title", "Article not found");
            return "article-not-found";
        }
        model.addAttribute("title",article.getTitle());
        model.addAttribute("article",article);
        return "article";
    }
}
