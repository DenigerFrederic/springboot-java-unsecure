package tech.capedev.springbootjavainsecure;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DbConfiguration {
    @Bean
    public ApplicationRunner databaseInitializer(final UserRepository userRepository, final ArticleRepository articleRepository) {
        return args -> {
            final User user = new User("user_login", "Foo", "Bar");
            userRepository.save(user);
            articleRepository.save(Article.create("First article", "Lorem ipsum", "dolor sit amet", user));
            articleRepository.save(Article.create("Second article", "Lorem ipsum", "dolor sit amet", user));
        };
    }
}
