package tech.capedev.springbootjavainsecure;

import javax.persistence.EntityManager;

public class UnsecureUserRepositoryImpl implements UnsecureUserRepository {
    private final EntityManager entityManager;

    public UnsecureUserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public User findByLoginUnsecure(String login) {
        String jql = "from User where login = '" + login + "'";
        return entityManager.createQuery(jql, User.class).getSingleResult();
    }
}
