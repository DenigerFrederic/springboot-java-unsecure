package tech.capedev.springbootjavainsecure;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long>, UnsecureUserRepository  {

    User findByLogin(String login);
}
