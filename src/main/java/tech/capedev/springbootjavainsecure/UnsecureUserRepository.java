package tech.capedev.springbootjavainsecure;

import org.springframework.data.repository.query.Param;

public interface UnsecureUserRepository {
    User findByLoginUnsecure(@Param("login") String login);
}
