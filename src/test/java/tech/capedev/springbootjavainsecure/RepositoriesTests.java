package tech.capedev.springbootjavainsecure;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoriesTests {
    @Autowired
    TestEntityManager entityManager;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ArticleRepository articleRepository;

    @Test
    public void testFindById() {
        User user = new User("login", "firstname", "lastname");
        entityManager.persist(user);
        Article article = Article.create("title", "headline", "content", user);
        entityManager.persist(article);
        entityManager.flush();
        Assert.assertNotNull(article.getId());
        Article found = articleRepository.findById(article.getId()).get();
        Assert.assertEquals(found, article);
    }

    @Test
    public void testFindByLogin() {
        User user = new User("login", "firstname", "lastname");
        entityManager.persist(user);
        entityManager.flush();
        User found = userRepository.findByLogin(user.getLogin());
        Assert.assertEquals(found, user);
    }
}
