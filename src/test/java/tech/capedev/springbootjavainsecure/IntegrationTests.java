package tech.capedev.springbootjavainsecure;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTests {
    @Autowired
    TestRestTemplate restTemplate;

    @Test
    public void testArticles() {
        final ResponseEntity<String> entity = restTemplate.getForEntity("/", String.class);
        Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
        Assert.assertTrue(entity.getBody().contains("<h1>[Springboot- Java] Articles with security issues</h1>"));
    }

    @Test
    public void testArticleSinglePage() {
        final ResponseEntity<String> entity = restTemplate.getForEntity("/article?slug=first-article", String.class);
        Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
        Assert.assertTrue(entity.getBody().contains("Lorem ipsum"));
    }
}
